# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()
        #print "legal moves: ",legalMoves

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)


        "*** YOUR CODE HERE ***"
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        score = successorGameState.getScore()

        # If ghost is 1/2 blocks away, reduce the score as Pacman doesnt want to go there
        if (util.manhattanDistance(newGhostStates[0].getPosition(), newPos) < 2):
            score -= 1000

        minimumDist = 0

        # For each successor, get the minimum food distance from the current position
        for food in successorGameState.getFood().asList():
            tempDist = util.manhattanDistance(food, newPos)
            if (minimumDist == 0):
                minimumDist = tempDist
            elif (tempDist < minimumDist):
                minimumDist = tempDist

        score += 20 - minimumDist

        # If successor position has Food, increase the score.
        if(currentGameState.getNumFood() > successorGameState.getNumFood()):
            score += 100

        return score


def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """


    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"

        ''' For Pacman, get the max value that is available from the next level (Ghosts)
            For each Ghosts, get the min value that is available from the next level (Pacman) '''

        # Get all the legal actions from the particular position
        legalMoves = gameState.getLegalActions()
        agent_state = self.index
        num_agents = gameState.getNumAgents()
        depth = self.depth

        # If game is lost or won from here, return the evaluation function (score) for that action
        if gameState.isLose() or gameState.isWin():
            scores = [self.evaluationFunction(gameState) for action in legalMoves]
            return scores

        # Calculate the max value from the next level for Pacman
        scores = self.max_value(gameState, depth, num_agents)

        return scores[1]


    def max_value(self, gameState, depth, num_agents):
        value = -99999 #float("-inf")
        i = 0
        max_action = ''

        # Get all the legal actions from the particular position
        legalMoves = gameState.getLegalActions(0)

        #If depth is reached or Pacman won/lost, return the score for that action
        if depth == 0 or gameState.isLose() or gameState.isWin():
            return self.evaluationFunction(gameState), Directions.STOP

        #Generate the successors for each corresponding action
        min_child = [gameState.generateSuccessor(0, each_action) for each_action in legalMoves]

        # For each successor position of Pacman, calculate the distance and pass the action
        # which has maximum distance
        for each_child in min_child:
            action = self.min_value(each_child, depth, num_agents, 1, num_agents-2)
            if action > value:
                value = action
                max_action = legalMoves[i]
            i += 1

        # Returns the action and cost for the action
        return (value, max_action)


    def min_value(self, gameState, depth, numAgents, agentIndex, range):
        value = 99999

        # Get all the legal actions from the particular position
        legalMoves = gameState.getLegalActions(agentIndex)

        # If depth is reached or Pacman won/lost, return the score for that action
        if depth == 0 or gameState.isLose() or gameState.isWin():
            return self.evaluationFunction(gameState)

        # Generate the successor specific to each legal actions
        max_child = [gameState.generateSuccessor(agentIndex, each_action) for each_action in legalMoves]

        if range == 0:
            # If it is the last ghost, then find the successor positions for that ghost
            # and move to the next level after that.
            for child in max_child:
                action = self.max_value(child, depth -1, numAgents)
                if action[0] < value:
                    value = action[0]

        else:
            # For each ghost, get the minimum action value and store that.
            # The action for which each ghost has minimum value will be passed to the upper level
            for child in max_child:
                action = self.min_value(child, depth, numAgents, (agentIndex+1)%numAgents, range-1)
                if action < value:
                    value = action

        # Return the minimum cost for that node.
        return value



class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"

        ''' For Pacman, get the max value that is available from the next level (Ghosts)
            For each Ghosts, get the min value that is available from the next level (Pacman)
            If for any successor, alpha > beta, then prune that part of the tree and return '''

        # Get all the legal actions from the particular position
        legalMoves = gameState.getLegalActions()
        agent_state = self.index
        num_agents = gameState.getNumAgents()
        depth = self.depth
        alpha = -99999
        beta = 99999

        # If game is lost or win, return the evaluation function (score) for that action
        if gameState.isLose() or gameState.isWin():
            scores = [self.evaluationFunction(gameState) for action in legalMoves]
            return scores

        # Calculate the max value from the next level for Pacman
        scores = self.max_value(gameState, depth, num_agents, alpha, beta)

        return scores[1]


    def max_value(self, gameState, depth, num_agents, alpha, beta):
        value = -99999
        i = 0
        max_action = ''

        # Get all the legal actions from the particular position
        legalMoves = gameState.getLegalActions(0)

        # If depth is reached or Pacman won/lost, return the score for that action
        if depth == 0 or gameState.isLose() or gameState.isWin():
            return self.evaluationFunction(gameState), Directions.STOP

        #min_child = [gameState.generateSuccessor(0, each_action) for each_action in legalMoves]

        # For each legal action, calculate the minimum value and pass it to Minimizer (Ghost)/ Root.
        for each_action in legalMoves:
            # For each successor position of Pacman, calculate the distance and pass the action
            # which has maximum distance
            action = self.min_value(gameState.generateSuccessor(0, each_action), depth, num_agents, 1, num_agents-2, alpha, beta)
            if action > value:
                value = action
                max_action = legalMoves[i]
                alpha = max(alpha, action)

                # If alpha > beta, prune the next level as minimizer will never select this part of the tree.
                if action > beta:
                    return action, max_action
            i += 1

        # Return the max_value along with its action
        return (value, max_action)


    def min_value(self, gameState, depth, numAgents, agentIndex, range, alpha, beta):
        value = 99999

        # Get all the legal actions from the particular position
        legalMoves = gameState.getLegalActions(agentIndex)

        # If depth is reached or Pacman won/lost, return the score for that action
        if depth == 0 or gameState.isLose() or gameState.isWin():
            return self.evaluationFunction(gameState)

        #max_child = [gameState.generateSuccessor(agentIndex, each_action) for each_action in legalMoves]

        # For each legal action, get the minimum value that is available and pass it to Maximizer (Pacman)
        for each_action in legalMoves:
            if range == 0:
                # If it is the last ghost, then find the successor positions for that ghost
                # and move to the next level after that
                action = self.max_value(gameState.generateSuccessor(agentIndex, each_action), depth -1, numAgents, alpha, beta)
                if action[0] < value:
                    value = action[0]
                beta = min(beta, value)

                # If beta < alpha, prune the tree and return, as Maximizer will not select this tree
                if value < alpha:
                    return value

            else:
                # For each ghost, get the minimum action value and store that.
                # The action for which each ghost has minimum value will be passed to the upper level
                action = self.min_value(gameState.generateSuccessor(agentIndex, each_action), depth, numAgents, (agentIndex+1)%numAgents, range-1, alpha, beta)
                if action < value:
                    value = action
                beta = min(beta, value)

                # If beta < alpha, prune the tree and return, as Maximizer will not select this tree
                if value < alpha:
                    return value

        return value




class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        ''' For Pacman, get the max value that is available from the next level (Ghosts)
            For each Ghosts, get the average value that is available from the next level (Pacman)'''

        # Get all the legal actions from the particular position
        legalMoves = gameState.getLegalActions()
        agent_state = self.index
        num_agents = gameState.getNumAgents()
        depth = self.depth

        # If game is lost or win, return the evaluation function (score) for that action
        if gameState.isLose() or gameState.isWin():
            scores = [self.evaluationFunction(gameState) for action in legalMoves]
            return scores

        # Calculate the max value from the next level for Pacman
        scores = self.max_value(gameState, depth, num_agents)

        return scores[1]


    def max_value(self, gameState, depth, num_agents):
        value = float("-inf")
        i = 0
        max_action = ''

        # Get all the legal actions from the particular position
        legalMoves = gameState.getLegalActions(0)

        # If depth is reached or Pacman won/lost, return the score for that action
        if depth == 0 or gameState.isLose() or gameState.isWin():
            return self.evaluationFunction(gameState), Directions.STOP

        # Generate the successor specific to each legal actions
        min_child = [gameState.generateSuccessor(0, each_action) for each_action in legalMoves]

        # For each legal action, calculate the minimum value and pass it to Minimizer (Ghost)/ Root.
        for each_child in min_child:
            # For each successor position of Pacman, calculate the distance and pass the action
            # which has maximum distance
            action = self.min_value(each_child, depth, num_agents, 1, num_agents - 2)
            if action > value:
                value = action
                max_action = legalMoves[i]
            i += 1

        # Return the max_value along with its action
        return (value, max_action)


    def min_value(self, gameState, depth, numAgents, agentIndex, range):
        value = float(0)
        i = float(0)
        avg_value = float(0)

        # Get all the legal actions from the particular position
        legalMoves = gameState.getLegalActions(agentIndex)

        # If depth is reached or Pacman won/lost, return the score for that action
        if depth == 0 or gameState.isLose() or gameState.isWin():
            return self.evaluationFunction(gameState)

        # Generate the successor specific to each legal actions
        max_child = [gameState.generateSuccessor(agentIndex, each_action) for each_action in legalMoves]

        # For each legal action, calculate the minimum value and pass it to Minimizer (Ghost)/ Root.
        for child in max_child:
            if range == 0:
                # If it is the last ghost, then find the successor positions for that ghost
                # and move to the next level after that
                action = self.max_value(child, depth - 1, numAgents)
                avg_value += action[0]

            else:
                # For each ghost, get the minimum action value and store that.
                # The action for which each ghost has minimum value will be passed to the upper level
                action = self.min_value(child, depth, numAgents, (agentIndex + 1) % numAgents, range - 1)
                avg_value += action

        # For expect node, return the average of each successor value instead of minimum value (incase of Minimizer)
        value = avg_value/len(max_child)

        return value


def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"


    util.raiseNotDefined()

# Abbreviation
better = betterEvaluationFunction

